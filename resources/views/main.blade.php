<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style media="screen">
    body{
      background-color:transparent;
      padding:0px;
      overflow: hidden;
    }

  </style>
  <title>Le Baccanti Safe</title>
</head>
<body>
  <div class="container p-0">
    <div class="row">
      <div class="col-md-4 offset-md-4">
        <div class="card bg-light">
          <div class="card-header">Credit card details</div>
          <div class="card-body">
            <form method="POST" action="/new_cc">
              <input type="hidden" name="data[tour_id]" value="{{$tour_id}}">
              <div class="form-row mt-2">
                <div class="col">
                  <input value="{{$cardholder}}" type="text" class="form-control" placeholder="Cardholder" required name="data[cardholder]">
                </div>
              </div>
              <div class="form-row mt-2">
                <div class="col">
                  <input type="text" class="form-control" placeholder="Credit card number" required name="data[num]">
                </div>
              </div>
              <div class="form-row mt-2">
                <div class="col">
                  <input maxlength="2" type="text" class="form-control" placeholder="MM" required name="data[exp_mm]">
                </div>
                <div class="col">
                  <input maxlength="2" type="text" class="form-control" placeholder="YY" required name="data[exp_yy]">
                </div>
              </div>
              <div class="form-row mt-2">
                <div class="col">
                  <label>
                    <input type="checkbox" required>
                    Please read & accept our <a target="_blank" href="/terms-and-conditions">terms and conditions</a> before proceeding
                  </label>
                </div>
              </div>
              <div class="form-row mt-2">
                <div class="col">
                  <button type="submit" class="btn btn-success btn-block">Confirm</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
