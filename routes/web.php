<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', function () {
    return redirect('admin');
});

#Route::get('/',function(){phpinfo();});

Route::get('/form/{tour_id?}/{locale?}/{cardholder?}', 'AdminCcController@ccForm');

Route::post('/new_cc','AdminCcController@newCc');

Route::get('/terms-and-conditions',function(){
  return view('terms_and_conditions');
});
